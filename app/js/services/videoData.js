videoApp.factory('videoData',  function($resource, $q) {
			return {
				processjson:  function(subscriber,id) {
					var deferred = $q.defer();
					//var TARGET_URL = '/data/AdminLibrary/services/rest/AdminManager/registerSubscriber/:id';
					var TARGET_URL = "http://localhost:8080/data/99/:id";
					var fixedTargetUrl = TARGET_URL.replace(/(:\d{4})/, "\\$1");
					var resource = $resource(fixedTargetUrl, {id:'@id'});
					//var resource = $resource(TARGET_URL, {id:'@id'});
					subscriber.id = id;
						resource.save(subscriber,
                			function(response, $window) { 
                				deferred.resolve(response); 
                				console.log(response);
                			},
                			function(response) { deferred.reject(response);}
            			);
            		bootbox.alert("Response HTML Page", 
            			function() {
        	 						window.location.href = "http://localhost:8080/index.html/#/result";            // Works Fine
        	 						});
            		return deferred.promise;
        		},
                getcompositionjson: function(subscriber,id) {
                    var deferred = $q.defer();
                    var TARGET_URL = "http://localhost:8080/data/:id";
                    var fixedTargetUrl = TARGET_URL.replace(/(:\d{4})/, "\\$1");
                        $resource(fixedTargetUrl, {id:'@id'})
                        .get({id: 99},
                    function (subscribers) {
                        deferred.resolve(subscribers);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                    return deferred.promise;
                }
	}
});