'use strict';

videoApp.controller('ResultController',
	function ResultController($scope, videoData) {
		$scope.duration = "3 minutes 50 seconds";
		$scope.vcount = 0;
		$scope.mpegcount =0;
		$scope.movcount =0;
		$scope.mp4count = 0;
		$scope.mp3count = 0;
		$scope.tcount = 0;
		$scope.icount = 0;
		$scope.jpegcount = 0;
		$scope.gifcount = 0;
		$scope.ccount = 0;
		$scope.acount = 0;

		$scope.subscribers = videoData.getcompositionjson(99);
    	$scope.subscribers.then(
            function(subscribers)   { console.log(subscribers); $scope.process(subscribers);},
            function(response) { console.log(response); }
        );

        $scope.msToTime = function(duration) {
        var milliseconds = parseInt((duration%1000)/100)
            , seconds = parseInt((duration/1000)%60)
            , minutes = parseInt((duration/(1000*60))%60)
            , hours = parseInt((duration/(1000*60*60))%24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
    }

        $scope.process = function(subscribers) {
			        var sub = subscribers.composition;
			        var total_len = sub.length;
			        var total_duration = 0;
			        for(var i = 0; i < total_len; i++)
			        {
			        	if(sub[i].duration > 0)
			        	{
			        		total_duration = total_duration + sub[i].duration;
			        	}	
			        	switch(sub[i].type)
			        	{
			        		case "video":
			        				$scope.vcount = $scope.vcount + 1;
			        				switch(sub[i].format)
			        				{
			        					case "mpeg":
			        							$scope.mpegcount = $scope.mpegcount + 1;
			        							break;
			        					case "mov":
			        							$scope.movcount = $scope.movcount + 1;
			        							break;
			        					case "mp4":
			        							$scope.mp4count = $scope.mp4count + 1;
			        							break;
			        					default:
			        							break;
			        				}
			        				break;
			        		case "audio":
			        				$scope.acount = $scope.acount + 1;
			        				switch(sub[i].format)
			        				{
			        					case "mp3":
			        							$scope.mp3count = $scope.mp3count + 1;
			        							break;
			        					default:
			        							break;
			        				}
			        				break;
			        		case "image":
			        			    $scope.icount = $scope.icount + 1;
			        			    switch(sub[i].format)
			        			    {
			        			    	case "jpeg":
			        			    			$scope.jpegcount = $scope.jpegcount + 1;
			        			    			break;
			        			    	case "gif":
			        			    			$scope.gifcount = $scope.gifcount + 1;
			        			    			break;
			        			    	default:
			        			    			break;
			        			    }
			        			    break;
			        		case "text":
			        				$scope.tcount = $scope.tcount + 1;
			        				break;
			        		case "composition":
			        				$scope.ccount = $scope.ccount + 1;
			        				$scope.process(sub[i]);
			        				break;
			        	}
			        }
			        console.log("video count :"+$scope.vcount);
			        console.log("audio count :"+$scope.acount);
			        console.log("image count :"+$scope.icount);
			        console.log("text count :"+$scope.tcount);
			        console.log("composition count :"+$scope.ccount);
			        $scope.duration = $scope.msToTime(total_duration);

        }

})