'use strict';

var videoApp = angular.module('videoApp', ['ngRoute','ngResource']);

videoApp.config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/landing',
            {
                templateUrl: '/templates/VideoComposer.html',
                controller: 'VideoComposerController'
            });
        $routeProvider.when('/result',
            {
                templateUrl: '/templates/result.html',
                controller:  'ResultController'

            });
    $routeProvider.otherwise({redirectTo: '/landing'});
 }]);